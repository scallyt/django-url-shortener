from django.urls import include, path
from rest_framework import routers

from users import views

app_name = 'users'



urlpatterns = [
    path('sign-in', views.signin),
    path('sign-up', views.signup),
    path('test', views.test_token),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
