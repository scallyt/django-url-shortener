import bcrypt
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated

import users
from .models import User
from .serializers import UserSerializer
from rest_framework import status

from django.shortcuts import get_object_or_404
from rest_framework.authtoken.models import Token

salt = bcrypt.gensalt()

@api_view(['POST'])
def signup(request):
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        user = serializer.save()
        password_bytes = request.data['password'].encode('utf-8')
        hashed_pw = bcrypt.hashpw(password_bytes, bcrypt.gensalt())
        user.password = hashed_pw.decode('utf-8')
        user.save()

        if not isinstance(user, User):
            return Response({'error': 'User instance is not valid.'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        try:
            token = Token.objects.create(user=user)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response({'token': token.key, 'user': serializer.data}, status=status.HTTP_201_CREATED)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def signin(request):
    user = get_object_or_404(User, username=request.data['username'])
    password_bytes = request.data['password'].encode('utf-8')
    stored_password_bytes = user.password.encode('utf-8')
    
    if not bcrypt.checkpw(password_bytes, stored_password_bytes):
        return Response("Invalid credentials", status=status.HTTP_404_NOT_FOUND)
    
    token, created = Token.objects.get_or_create(user=user)
    serializer = UserSerializer(user)
    return Response({'token': token.key, 'user': serializer.data})


@api_view(['GET'])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes([IsAuthenticated])
def test_token(request):
    return Response("passed!")