from rest_framework import serializers
from .models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = User
        fields = '__all__'
        extra_kwargs = {
            'password': {'write_only': True}
        }