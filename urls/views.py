import random
import string
from django.shortcuts import get_object_or_404, redirect
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from users.models import User
from .serializers import UrlSerializer
from .models import Url

def generate_short_url():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))

@api_view(['POST', 'PUT', 'DELETE'])
def new_url(request, id=None):
    if request.method == 'POST':
        user = get_object_or_404(User, id=request.data.get('user'))
        if user.urls_have >= 10:
            return Response({'error': 'A user cannot have more than 10 URLs.'}, status=status.HTTP_400_BAD_REQUEST)

        base_url = request.data.get('base_url')
        existing_url = Url.objects.filter(user=user, base_url=base_url).first()
        if existing_url:
            return Response({"message": "This urls has ben created", 'short_url': f"http://localhost:8080/urls/alma/{existing_url.short_url}"}, status=status.HTTP_200_OK)

        serializer = UrlSerializer(data=request.data)
        if serializer.is_valid():
            url = serializer.save()
            short_url = generate_short_url()
            url.short_url = short_url
            url.save()

            user.urls_have = Url.objects.filter(user=user).count()
            user.save()

            return Response({'url': UrlSerializer(url).data, 'short_url': f"http://localhost:8080/urls/alma/{short_url}"}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'PUT':
        if id is None:
            return Response({'error': 'ID parameter is required for PUT method'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            url = Url.objects.get(pk=id)
        except Url.DoesNotExist:
            return Response({'error': 'URL not found'}, status=status.HTTP_404_NOT_FOUND)
        
        base_url = request.data.get('base_url')
        existing_url = Url.objects.filter(user=url.user, base_url=base_url).exclude(pk=id).first()
        if existing_url:
            return Response({'error': 'Base URL already exists for this user'}, status=status.HTTP_400_BAD_REQUEST)

        serializer = UrlSerializer(url, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'DELETE':
        if id is None:
            return Response({'error': 'ID parameter is required for DELETE method'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            url = Url.objects.get(pk=id)
        except Url.DoesNotExist:
            return Response({'error': 'URL not found'}, status=status.HTTP_404_NOT_FOUND)
        
        url.delete()

        user = url.user
        user.urls_have = Url.objects.filter(user=user).count()
        user.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    return Response({'error': 'Method not allowed'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['GET'])
def redirect_url(request, shorturl=None):
    if shorturl is None:
        return Response({'error': 'Error'}, status=status.HTTP_400_BAD_REQUEST)

    try:
        url = Url.objects.get(short_url=shorturl)
    except Url.DoesNotExist:
        return Response({'error': 'URL not found'}, status=status.HTTP_404_NOT_FOUND)
    
    return redirect(url.base_url)