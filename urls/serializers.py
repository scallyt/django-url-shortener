import random
import string
from rest_framework import serializers
from .models import Url

def generate_short_url():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))

class UrlSerializer(serializers.ModelSerializer):
    class Meta:
        model = Url
        fields = ['user', 'base_url']
