from django.urls import path
from . import views

app_name = 'urls'

urlpatterns = [
    path("alma", views.new_url, name="test"),
    path("alma/<int:id>", views.new_url, name="test-detail"),
    path("alma/<slug:shorturl>", views.redirect_url, name="redirect")
]