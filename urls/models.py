from django.db import models
from users.models import User

class Url(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    base_url = models.CharField(max_length=255)
    short_url = models.CharField(max_length=255)

    def __str__(self):
        return self.short_url
